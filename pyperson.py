#!/usr/bin/python2
# Pyperson; little program to display photos/audio for Ben as a going-away gift
from __future__ import division
import sys, os, pygame, random

RUNNING_ON_RPI = False

if RUNNING_ON_RPI:
    import RPi.GPIO as GPIO
    DEBUG = 0
else:
    DEBUG = 10

# Globals
SCREEN_W = 320
SCREEN_H = 240

EV_AUDIO_END = pygame.USEREVENT + 1
EV_NEXT_PHOTO = pygame.USEREVENT + 2
EV_GPIO_A = pygame.USEREVENT + 3
EV_GPIO_B = pygame.USEREVENT + 4
EV_GPIO_C = pygame.USEREVENT + 5

# Shared music; file path and latest time at which to start randomly playing
background_music = []
background_music.append( ("shared/benny_hill.mp3", 270, 0.14))
background_music.append( ("shared/monty_pythons_flying_circus.mp3", 10, 0.2))
background_music.append( ("shared/god_save_the_queen.mp3", 140, 0.07))
background_music.append( ("shared/tank_girl.mp3", 30, 0.3))
background_music.append( ("shared/classical.ogg", 180, 0.2))
background_music.append( ("shared/mozart_rondo.mp3", 100, 0.2))
background_music.append( ("shared/moonlight_sonata_3rd.mp3", 100, 0.2))

# Utility functions

def debug_print(str, level):
    if DEBUG >= level:
        print(str)

def image_scale(img, height=SCREEN_H, width=SCREEN_W):
    img_w, img_h  = img.get_size()
    w_scale, h_scale = width/img_w, height/img_h
    scaling = min(w_scale, h_scale)
    img_scaled = pygame.transform.\
                 smoothscale(img, (int(img_w*scaling), int(img_h*scaling)))
    return img_scaled

def generate_password(length):
    pwd = ""
    for k in xrange(length):
        if random.randint(0,1):
            pwd += "B"
        else:
            pwd += "C"
    
    return pwd

def play_background_music():
    song = random.choice(background_music)
    pygame.mixer.music.load(song[0])
    pygame.mixer.music.set_volume(song[2])
    pygame.mixer.music.play(0, song[1]*random.random())

class Scene:
    def __init__(self):
        self.next = self
    
    def processEvents(self, events):
        print("uh-oh, you didn't override this in the child class")

    def update(self):
        print("uh-oh, you didn't override this in the child class")

    def render(self, screen):
        print("uh-oh, you didn't override this in the child class")

    def switchToScene(self, next_scene):
        self.next = next_scene
    
    def terminate(self):
        self.switchToScene(None)

class DisplayScene(Scene):
    """ Info display for Ben """
    def __init__(self, title, lines, nextScene=None, prevScene=None):
        Scene.__init__(self)
        # Title 
        self.titleFont = pygame.font.SysFont("comicsansbold", 26)
        self.messageFont = pygame.font.SysFont("georgia", 18)
        self.titleText = self.titleFont.render(title, True, (100,200,40))
        self.linesText = [self.messageFont.render(l, True, (130, 150, 255)) for l in lines]
        self.path = "displays/" + title.split(" ")[0] + "/"
        self.splash = image_scale(pygame.image.load(self.path + "splash.jpg"), SCREEN_H)
        self.setNeighbours(nextScene, prevScene)

    def setNeighbours(self, nextScene, prevScene):
        self.nextScene = nextScene
        self.prevScene = prevScene

    def processEvents(self, events):
        if not pygame.mixer.music.get_busy():
            play_background_music()
        for event in events:
            if event.type == EV_GPIO_A \
               or event.type == EV_GPIO_B \
               or event.type == EV_GPIO_C:
                debug_print("Button pressed; moving to next slide", 3)
                pygame.mixer.music.stop()
                self.switchToScene(self.nextScene)

    def update(self):
        pass

    def render(self, screen):
        screen.fill((10,40,20))
        screen.blit(self.splash, (0,0))
        screen.blit(self.titleText, ( (SCREEN_W - self.titleText.get_width()) // 2, 0))
        x_start = 10
        y_start = self.titleText.get_height() + 5
        y_separation = 3
        for l in self.linesText:
            screen.blit(l, (x_start, y_start))
            y_start += l.get_height() + y_separation

class PersonScene(Scene):
    """ Personnel page info"""
    def __init__(self, person, password_len, photoTimes, \
                 personTextCol=(255,255,255), \
                 passwordTextCol=(255,40,0), \
                 hintTextCol=(150,150,150), \
                 nextScene=None, prevScene=None):
        Scene.__init__(self)
        self.personTextCol = personTextCol
        self.passwordTextCol = passwordTextCol
        self.hintTextCol = hintTextCol
        self.playing_message = False
        self.error_paths = []
        self.error_paths_played = []
        self.photos = []
        self.photo_idx = 0
        self.splash_h_offset = 0
        self.splash_h_vel = 5
        self.person = person
        self.path = "people/" + person.split(" ")[0] + "/"
        self.password_len = password_len
        self.password = generate_password(self.password_len)
        self.password_attempt = ""
        self.photoTimes = [int(p * 1000) for p in photoTimes]
        self.max_photo_idx = len(self.photoTimes)
        self.load_media()
        self.bigFont = pygame.font.SysFont("georgiabold", 23)
        self.medFont = pygame.font.SysFont("georgia", 16)
        self.littleFont = pygame.font.SysFont("times", 14)
        self.setNeighbours(nextScene, prevScene)
        
        # Text stuff
        self.rerender_all()

    def rerender_all(self):
        self.personText = self.bigFont.render(self.person, True, self.personTextCol)
        self.rerender_password()
        self.rerender_password_attempt()
        self.hintText = self.littleFont.render("A: next person; B/C: enter password", True, self.hintTextCol)

    def rerender_password(self):
        self.passwordText = self.medFont.render("Password: " + self.password, True, self.passwordTextCol)
    def rerender_password_attempt(self):
        self.passwordAttemptText = self.medFont.render(self.password_attempt, True, self.passwordTextCol)

    def setNeighbours(self, nextScene, prevScene):
        self.nextScene = nextScene
        self.prevScene = prevScene

    def processEvents(self, events):
        for event in events:
            if self.playing_message:
                # Wait for timeout on a photo before switching to next
                # one, or end of music.
                if event.type == EV_AUDIO_END:
                    debug_print("Audio finished; stopping playback", 3)
                    self.playing_message = False
                    self.photo_idx = 0
                    play_background_music()
                elif event.type == EV_NEXT_PHOTO:
                    debug_print("Next photo", 3)
                    self.photo_idx += 1
                    if self.photo_idx < self.max_photo_idx:
                        pygame.time.set_timer(EV_NEXT_PHOTO, self.photoTimes[self.photo_idx])
                    else:
                        pygame.time.set_timer(EV_NEXT_PHOTO, 0)
                        debug_print("Last photo", 3)
                elif event.type ==  EV_GPIO_A \
                   or event.type == EV_GPIO_B \
                   or event.type == EV_GPIO_C:
                    debug_print("Button pressed; stopping playback", 3)
                    pygame.time.set_timer(EV_NEXT_PHOTO, 0)
                    pygame.mixer.music.stop()
                    self.playing_message = False
                    self.photo_idx = 0
                
            else:
                if event.type == EV_GPIO_A:
                    self.password_attempt = ""
                    self.rerender_password_attempt()
                    debug_print("You pressed A; switching to next person", 3)
                    pygame.mixer.music.stop()
                    self.switchToScene(self.nextScene)
                elif event.type == EV_GPIO_B:
                    self.password_attempt += "B"
                    self.rerender_password_attempt()
                elif event.type == EV_GPIO_C:
                    self.password_attempt += "C"
                    self.rerender_password_attempt()                
                
                if len(self.password_attempt) == self.password_len:
                    pygame.mixer.music.stop()
                    if self.password_attempt == self.password:
                        # Start playing a message; prepare timer to switch photos later
                        debug_print("Password correct; starting to play message", 3)
                        self.playing_message = True
                        pygame.mixer.music.load(self.path + "message.mp3")
                        pygame.mixer.music.set_volume(1.0)
                        pygame.mixer.music.play(0)
                        if len(self.photoTimes) is not 0: # if more than 1 photo
                            pygame.time.set_timer(EV_NEXT_PHOTO, self.photoTimes[self.photo_idx])
                    else:
                        debug_print("Password incorrect!", 3)
                        pygame.mixer.music.stop()
                        # Random error message
                        if len(self.error_paths) is 0:
                            self.error_paths = self.error_paths_played
                            self.error_paths_played = []
                        error_to_play = random.randint(0, len(self.error_paths)-1)
                        pygame.mixer.music.load(self.error_paths[error_to_play])
                        pygame.mixer.music.set_volume(1.0)
                        pygame.mixer.music.play(0)
                        self.error_paths_played.append(self.error_paths[error_to_play])
                        del self.error_paths[error_to_play]
                    self.password_attempt = ""
                    self.password = generate_password(self.password_len)
                    self.rerender_password()
                    self.rerender_password_attempt()
                elif not pygame.mixer.music.get_busy():
                    # by default, play background music
                    play_background_music()

    def update(self):
        # Update internal page stuff
        if self.playing_message:
            pass
        else:
            if self.splash_h_offset > SCREEN_W-self.splash.get_width():
                self.splash_h_vel *= -1
            elif self.splash_h_offset < 0:
                self.splash_h_vel *= -1
            self.splash_h_offset += self.splash_h_vel

    def render(self, screen):
        if self.playing_message:
            screen.fill((0,0,0))
            photo = self.photos[self.photo_idx]
            photo_w, photo_h = photo.get_width(), photo.get_height()
            screen.blit(photo, ((SCREEN_W-photo_w)//2,(SCREEN_H-photo_h)//2))
        else:
            screen.fill((0,0,0))
            # Get image dimensions
            screen.blit(self.splash, (self.splash_h_offset, 25))
            screen.blit(self.personText, ( (SCREEN_W - self.personText.get_width()) // 2, 0))
            screen.blit(self.passwordText, (10, SCREEN_H - 60))
            screen.blit(self.passwordAttemptText, (10, SCREEN_H - 40))
            screen.blit(self.hintText, (10, SCREEN_H - 20))
        
    def load_media(self):
        # Load splash screen
        self.splash = image_scale(pygame.image.load(self.path + "splash.jpg"), SCREEN_H-50)
        
        # Loop through photos and load as many as you can
        photo_idx = 1
        while(True):
            try:
                photo = image_scale(pygame.\
                                    image.load(self.path + "img" + str(photo_idx) + ".jpg"))
                self.photos.append(photo)
                photo_idx += 1
            except pygame.error:
                num_photos = photo_idx-1 # otherwise thinks an extra photo loaded
                break
        
        assert num_photos == len(self.photoTimes) + 1, "Photos: %d, phototimes: %d"%(num_photos, len(self.photoTimes))
        debug_print("Photos loaded for " + self.person + ": " + str(num_photos), 2)

        # Loop through error messages and load as many as you can
        error_idx = 1
        while(True):
            try:
                error_path = self.path + "err" + str(error_idx) + ".mp3"
                open(error_path, "r")
                self.error_paths.append(error_path)
                error_idx += 1
            except IOError:
                break

        debug_print("Error messages loaded for " + self.person + \
                    ": " + str(error_idx-1), 2)

           
def gpioApressed(channel):
    if RUNNING_ON_RPI:
        pygame.event.post(pygame.event.Event(EV_GPIO_A))
    debug_print("GPIO A pressed", 3)

def gpioBpressed(channel):
    if RUNNING_ON_RPI:
        pygame.event.post(pygame.event.Event(EV_GPIO_B))
    debug_print("GPIO B pressed", 3)

def gpioCpressed(channel):
    if RUNNING_ON_RPI:
        pygame.event.post(pygame.event.Event(EV_GPIO_C))
    debug_print("GPIO C pressed", 3)

def run_game(fps=15):
    os.environ["SDL_FBDEV"] = "/dev/fb1"
    pygame.init()
    if RUNNING_ON_RPI:
        screen = pygame.display.set_mode((SCREEN_W, SCREEN_H), pygame.FULLSCREEN)
    else:
        screen = pygame.display.set_mode((SCREEN_W, SCREEN_H))
    clock = pygame.time.Clock()

    pygame.mixer.music.set_endevent(EV_AUDIO_END)
    pygame.mouse.set_visible(0)

    # GPIO setup
    if RUNNING_ON_RPI:             
        #GPIO.setmode(GPIO.BCM)
        GPIO.setmode(GPIO.BCM)
        
        GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(22, GPIO.FALLING, callback=gpioApressed, bouncetime=300)
        GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(27, GPIO.FALLING, callback=gpioBpressed, bouncetime=300)
        GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(18, GPIO.FALLING, callback=gpioCpressed, bouncetime=300)   
        
    instructionScenes = []
    scenes = []
    # First info scene
    instructionScenes.append(DisplayScene("Hello Ben!", ["Each person has a page, whose message" \
                                                         ,"you can unlock by entering the page" \
                                                         ," password using the buttons"  
                                                         ,"","near the bottom-left of the screen. " \
                                                         ,"From left to right, they are" \
                                                         ," X, A, B and C.","", "Press A, B or C to continue."]))
    instructionScenes.append(DisplayScene("Buttons", ["A changes person, B and C enter " \
                                                      ,"the page password, and X reboots" \
                                                      ,"this system." \
                                                      ,"You may get the password wrong a few" \
                                                      ,"times on each page, but this" \
                                                      ," is intended!" \
                                                      , "","Press A, B or C now."]))

    # scenes.append(PersonScene("Test McGee", 3, [2,3,4]))
    # scenes.append(PersonScene("Test Steve", 4, [1,2,3]))
    # scenes.append(PersonScene("Test Mcqueen", 5, [1,1,1]))
    
    scenes.append(PersonScene("Chi Zhang", 8, []))
    scenes.append(PersonScene("Christa Fluehmann", 14, [7.2,5.3,2.6,6.9,11,3], (255,255,255), (50,200,150)))
    scenes.append(PersonScene("Christoph Fischer", 8, [30]))
    scenes.append(PersonScene("Florian Leupold", 14, [6,6,6,6,5]))
    scenes.append(PersonScene("Frieder Lindenfelser", 14, [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.25,0.25,0.25,0.25]))
    scenes.append(PersonScene("Joseba Alonso", 14, [15,26,16]))
    scenes.append(PersonScene("Hsiang-Yu Lo", 14, [3.5,3.5,3.5,3.5,3.5,3.5,3.5,3.5,3.5]))
    scenes.append(PersonScene("Ludwig de Clercq", 20, [8,8,8,8]))
    scenes.append(PersonScene("Martin Neugebauer", 8, [3,3]))
    scenes.append(PersonScene("Matteo Marinelli", 8, [5,5,5,4,4,4,3,3,3,2,2,2,1,1,1,0.5,0.5,0.5,0.5,0.5,0.5,0.5]))
    scenes.append(PersonScene("Mirjam Bruttin", 8, [12,9,7]))
    scenes.append(PersonScene("Nelson Darkwah Oppong", 14, [0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,2,2,2,1.2,1.2,1.2,1.2,1.2,1.2,2,3,3,5]))
    scenes.append(PersonScene("Ursin Soler", 14, [13,10,2,2,5,11,5], (255,255,255), (50,255,20)))
    scenes.append(PersonScene("Vlad Negnevitsky", 20, [6,6,12,7.5,7.5,7.5,4.5,1.2,1.2,1.2,1.2,1.2,0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3,1,1.5]))

    # Tie each scene with its previous and next scenes
    for k, s in enumerate(scenes):
        s.setNeighbours(scenes[k-1], scenes[k-len(scenes)])
    
    for k, s in enumerate(instructionScenes):
        s.setNeighbours(instructionScenes[k-1], instructionScenes[k-len(instructionScenes)])

    instructionScenes[-1].setNeighbours(scenes[0], instructionScenes[-2])

    active_scene = instructionScenes[0]

    pygame.event.set_allowed(EV_GPIO_A)

    while active_scene != None:
        pressed_keys = pygame.key.get_pressed()
        
        # Event filtering; clear list
        filtered_events = []
        for event in pygame.event.get():
            quit_attempt = False
            if event.type == pygame.QUIT:
                quit_attempt = True
            elif event.type == pygame.KEYDOWN:
                alt_pressed = pressed_keys[pygame.K_LALT] or \
                              pressed_keys[pygame.K_RALT]
                ctrl_pressed = pressed_keys[pygame.K_LCTRL] or \
                              pressed_keys[pygame.K_RCTRL]
                if event.key == pygame.K_ESCAPE:
                    quit_attempt = True
                elif event.key == pygame.K_F4 and alt_pressed:
                    quit_attempt = True
                elif event.key == pygame.K_c and ctrl_pressed:
                    quit_attempt = True

                elif event.key == pygame.K_a:
                    filtered_events.append(pygame.event.Event(EV_GPIO_A))
                    #pygame.event.post(pygame.event.Event(EV_GPIO_A))
                elif event.key == pygame.K_b:
                    filtered_events.append(pygame.event.Event(EV_GPIO_B))
                elif event.key == pygame.K_c:
                    filtered_events.append(pygame.event.Event(EV_GPIO_C))
            
            if quit_attempt:
                active_scene.terminate()
            else:                
                filtered_events.append(event)
        
        active_scene.processEvents(filtered_events)
        active_scene.update()
        active_scene.render(screen)
        
        old_scene = active_scene
        active_scene = active_scene.next
        old_scene.next = old_scene
        
        pygame.display.flip()
        clock.tick(fps)
        
    if RUNNING_ON_RPI:
        GPIO.cleanup()

if __name__ == "__main__":
    run_game()
