Experimentation with Pygame, on the RPi, to be a farewell gift for Ben once everyone gives audio + pictures.


In the tests folder:

pong.py : basic Pong game; following tutorial at http://trevorappleton.blogspot.co.uk/2014/04/writing-pong-using-python-and-pygame.html

bouncy_ball.py : hello-world-style Pygame example
